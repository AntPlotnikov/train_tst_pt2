package net.antplotnikov.helpers;

import org.openqa.selenium.*;

import static org.testng.Assert.assertFalse;

public class ProjectHelper extends HelperBase {
    protected StringBuffer verificationErrors;
    private boolean acceptNextAlert = true;

    public  ProjectHelper(WebDriver driver, StringBuffer verificationErrors){
        super(driver);
        this.verificationErrors = verificationErrors;
    }

    public void verifyNotChecked(By locator) {
        try {
            assertFalse(driver.findElement(locator).isSelected());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    public void verifyChecked(By locator) {
        try {
            assertFalse(driver.findElement(locator).isSelected());
        } catch (Error e) {
            verificationErrors.append(e.toString());
        }
    }

    private boolean isElementPresent(By by) {
        try {
            driver.findElement(by);
            return true;
        } catch (NoSuchElementException e) {
            return false;
        }
    }

    private boolean isAlertPresent() {
        try {
            driver.switchTo().alert();
            return true;
        } catch (NoAlertPresentException e) {
            return false;
        }
    }

    private String closeAlertAndGetItsText() {
        try {
            Alert alert = driver.switchTo().alert();
            String alertText = alert.getText();
            if (acceptNextAlert) {
                alert.accept();
            } else {
                alert.dismiss();
            }
            return alertText;
        } finally {
            acceptNextAlert = true;
        }
    }
}
