package net.antplotnikov.helpers;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.Select;

public class HelperBase {
    WebDriver driver;

    public HelperBase(WebDriver driver) {
        this.driver = driver;
    }

    public void clickId(By id) {
        driver.findElement(id).click();

    }

    public void click(By locator) {
        driver.findElement(locator).click();
    }

    public void click(By locator, String text){
        new Select(driver.findElement(locator)).selectByVisibleText(text);
    }
    public void clearField(By locator) {
        driver.findElement(locator).clear();
    }

    public void type(By locator, String text) {
            clearField(locator);
            driver.findElement(locator).sendKeys(text);
    }

    public void type(By locator, int i) {
        clearField(locator);
        driver.findElement(locator).sendKeys(Integer.toString(i));
    }

    public String getAttribute(By locator){
        return driver.findElement(locator).getAttribute("value");
    }
}
