package net.antplotnikov.autotest;

import net.antplotnikov.TestData.User;
import net.antplotnikov.TestData.UserBuilder;
import net.antplotnikov.helpers.AppManager;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;



public class TestBase {
protected final AppManager appManager = new AppManager();
    @BeforeClass(alwaysRun = true)
    public void setUp() throws Exception {
        appManager.init();
    }

    @AfterClass(alwaysRun = true)
    public void tearDown() throws Exception {
        appManager.stop();
    }


    User test3 = new UserBuilder().setLogin("test3").setPass("test3").createUser();
    User abs = new UserBuilder().setLogin("123").setPass("123").createUser();
}
