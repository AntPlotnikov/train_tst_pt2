package net.antplotnikov.autotest;

import net.antplotnikov.TestData.OfshoreCountry;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

public class VienaAuto extends TestBase {
    @Test
    public void testUntitled() throws Exception {
        appManager.getNavigationHelper().login(appManager.getNavigationHelper().getEnviroment(), test3);
        appManager.getNavigationHelper().findClient();
        appManager.getNavigationHelper().toOfshoreClient();
        appManager.getProjectHelper().verifyNotChecked(By.id("s_1_1_20_0"));
        appManager.getProjectHelper().verifyNotChecked(By.id("s_1_1_9_0"));
        appManager.getNavigationHelper().toClientDetail();
        appManager.getPageHelper().type(By.id("s_1_1_75_0"), new OfshoreCountry().getRandomOfCountry());
        appManager.getNavigationHelper().toOfshoreClient();
        appManager.getProjectHelper().verifyChecked(By.id("s_1_1_20_0"));
        appManager.getProjectHelper().verifyNotChecked(By.id("s_1_1_9_0"));
        appManager.getNavigationHelper().toClientDetail();
        appManager.getPageHelper().clearField(By.id("s_1_1_75_0"));
        appManager.getPageHelper().type(By.id("s_1_1_97_0"), new OfshoreCountry().getRandomOfCountry());
        appManager.getPageHelper().saveChanges();
        appManager.getNavigationHelper().toOfshoreClient();
        appManager.getProjectHelper().verifyChecked(By.id("s_1_1_20_0"));
        appManager.getProjectHelper().verifyNotChecked(By.id("s_1_1_9_0"));
        appManager.getNavigationHelper().toClientDetail();
        appManager.getPageHelper().clearField(By.id("s_1_1_97_0"));
        appManager.getPageHelper().saveChanges();
        appManager.getNavigationHelper().toOfshoreClient();
        appManager.getProjectHelper().verifyChecked(By.id("s_1_1_20_0"));
        appManager.getProjectHelper().verifyNotChecked(By.id("s_1_1_9_0"));
    }


}
