package net.antplotnikov.TestData;


import java.util.ArrayList;
import java.util.List;

public class OfshoreCountry {
    private List<String> ofCountries;

    public String getRandomOfCountry(){
        if(ofCountries == null){
            ofCountries = new ArrayList<>();
            ofCountries.add("Андорра");
            ofCountries.add("Ангилья");
            ofCountries.add("Багамы");
            ofCountries.add("Барбадос");
            ofCountries.add("Белиз");
            ofCountries.add("Бермудские острова");
            ofCountries.add("Британские Виргинские о-ва");
            ofCountries.add("Кайман Острова");
            ofCountries.add("Гибралтар");
            ofCountries.add("Острова Кука");
            ofCountries.add("Коста-Рика");
            ofCountries.add("Кипр");
            ofCountries.add("Доминика");
            ofCountries.add("о-ва Гернси");
            ofCountries.add("о-в Мэн");
            ofCountries.add("Джерси");
            ofCountries.add("Лабуан");
        }
        return ofCountries.get((int)Math.random()*16);
    }
}


