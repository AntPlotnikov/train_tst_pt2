package net.antplotnikov.helpers;

import net.antplotnikov.TestData.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;


public class NavigationHelper extends HelperBase {

    private Properties properties;
    private String enviroment;

    public NavigationHelper(WebDriver driver){
        super(driver);
    }

    public void login(String env, User a) {
        driver.get(env);
        type(By.id("s_swepi_1"), a.getLogin());
        type(By.id("s_swepi_2"), a.getPass());
        click(By.id("s_swepi_22"));
    }

    public String getEnviroment() throws IOException {
        properties = new Properties();
        properties.load(new FileInputStream("src\\test\\resources\\local.properties"));
        enviroment = properties.getProperty(System.getProperty("env","env.test"));
        return enviroment;
    }

    public void toClientDetail() {
        click(By.xpath("//a[contains(text(),'Детали Клиента')]"));
        click(By.xpath("//a[contains(text(),'Детали Клиента 2')]"));
    }

    public void goToLead() {
        click(By.id("ui-id-127"));
        click(By.id("ui-id-189"));
        click(By.linkText("1-725RQA1"));
    }

    public void goToLeadForm() {
        click(By.id("ui-id-128"));
        click(By.name("s_vis_div"), "Мои корпоративные лиды");
        click(By.xpath("//div[@id='s_vis_div']/select/option[2]"));
        click(By.id("s_1_1_0_0_Ctrl"));
        click(By.id("s_1_1_2_0_Ctrl"));
        click(By.id("s_1_1_2_0_Ctrl"));
    }

    public void toOfshoreClient() {
        click(By.xpath("//a[contains(text(),'Комплаенс')]"));
        click(By.xpath("//a[contains(text(),'Офшорный клиент')]"));
    }

    public void findClient() {
        click(By.linkText("Клиенты ЮЛ"));
        click(By.linkText("ЮЛ МнеБыВНебо7"));
    }

    public void finishLeadCeation() {
        click(By.id("s_1_1_2_0_Ctrl"));
        click(By.id("1_s_2_l_RI_Selected"));
        click(By.id("1_RI_Selected"));
        click(By.id("2_s_2_l_RI_Selected"));
        click(By.id("2_RI_Selected"));
        click(By.id("s_1_1_2_0_Ctrl"));
        click(By.name("s_1_1_0_0"));
        click(By.id("s_2_1_2_0_Ctrl"));
    }

    public void toLeadList() {
        click(By.name("s_vis_div"), "Мои лиды");
        click(By.cssSelector("select[name=\"s_vis_div\"] > option"));
    }
}
