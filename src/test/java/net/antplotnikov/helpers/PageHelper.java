package net.antplotnikov.helpers;

import net.antplotnikov.TestData.LeadForm;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;


public class PageHelper extends HelperBase{

    public PageHelper(WebDriver driver){
        super(driver);
    }

    public void fillLeadForm(LeadForm leadForm) {
        type(By.name("s_2_1_5_0"),leadForm.getFullName());
        type(By.name("s_2_1_7_0"),leadForm.getContactName());
        type(By.name("s_2_1_10_0"),leadForm.getEmail());
        type(By.name("s_2_1_12_0"),leadForm.getInn());
        type(By.name("s_2_1_14_0"),leadForm.getKpp());
    }

    public void saveChanges() {
        click(By.xpath("//button[@id='s_at_m_1']"));
        click(By.xpath("(//a[contains(text(),'Сохранить запись           [Ctrl+S]')])[2]"));
    }
}
