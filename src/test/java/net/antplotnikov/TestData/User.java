package net.antplotnikov.TestData;

/**
 * Created by ostgdd1 on 11.04.2017.
 */
public class User {
    private String login;
    private String pass;

    public User(String login,String pass){
        this.login=login;
        this.pass=pass;
    }

    public String getLogin(){
        return this.login;
    }

    public String getPass(){
        return this.pass;
    }

}
